"""
overriding and super()

"""


class A:
    varA = "class A variable"

    def __init__(self):
        self.var1 = "Inside  Class A constructor"
        self.varA = "Instance variable of Class A"


class B(A):
    varB = "class B variable"
    var33 = 10

    def __init__(self):
        super().__init__()
        self.var1 = "B's constructor hai"
        self.VARB = "B ka variable"
        print(self.varA)

    def printvarA(self):
        print(var33)


def main():
    a = A()
    b = B()

    b.printvarA()
    # print(super().varA, b.VARB)


if __name__ == "__main__":
    main()
