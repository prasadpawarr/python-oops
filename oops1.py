class Employee:
    no_of_holidays = 10

    def __init__(self, name="defa", age=10):
        self.name = name
        self.age = age


def main():
    emp1 = Employee("prasad", 20)
    emp2 = Employee()

    print(emp1.name, " ", emp1.age)
    print(emp2.name, " ", emp2.age)


if __name__ == '__main__':
    main()
