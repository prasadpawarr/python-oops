from oops2 import *


class Player(object):
    """docstring for Player."""

    def __init__(self, name, game):
        self.name = name
        self.game = game

    def printPlayer(self):
        return f"Name = {self.name} , Game = {self.game}"


class CoolProgrammer(Employee, Player):
    pass


def main():
    # emp = Employee("hi", 10, "2lac")
    prassy = Player("JonSnow", "NightsWatch")
    pooh = CoolProgrammer()
    print(prassy.printPlayer())


if __name__ == '__main__':
    main()
