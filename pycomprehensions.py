"""
List comprehensions
"""

# To get a list of numbers divisible by 4

div4 = [i for i in range(1, 25) if i % 4 == 0]
print(div4)


# --------------------------------------------------------------------
"""
Dictionary comprehensions
"""

dict1 = {i: f"item{i}" for i in range(1, 5)}
print("DICT = ", dict1)

# reversing the key-value using comprehensions

dict2 = {value: key for key, value in dict1.items()}
print("Reverse = ", dict2)
