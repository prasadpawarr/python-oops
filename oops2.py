class Employee:
    no_of_holidays = 10

    def __init__(self, name, age, salary):
        self.name = name
        self.age = age
        self.salary = salary

    def printDetails(self):
        return f"Name = {self.name}, Age = {self.age}, Salary = {self.salary}"

    @classmethod
    def changeLeaves(cls, new_holidays):
        cls.no_of_holidays = new_holidays

    @classmethod
    def fromStr(cls, obj_str):
        params = obj_str.split("-")
        return cls(params[0], params[1], params[2])

    @staticmethod
    def printSomething(str):
        print("this is a good thing " + str)
        return


def main():
    emp1 = Employee("prasad", 20, "20,000")
    emp2 = Employee("poonam", 24, "30,000")

    emp3 = Employee.fromStr("rohan-30-10,000")

    print(emp1.printDetails())
    print(emp2.printDetails())
    print(emp3.printDetails())

    emp1.printSomething("MEH")


if __name__ == '__main__':
    main()
